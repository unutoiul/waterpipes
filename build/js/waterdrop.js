var waterdrop = {
    data: {
        svg: document.getElementById('water_drip_svg'), 
        tl: new TimelineLite(),
        clientHeight: $(window).height(),
        scrollTop: $(window).scrollTop(),
        time: 0.5,
        delay: '-=0.2',
        reset: false,
        scrollMoved: false,
        ripplesStarted: false,
        aniStarted: false,
        animateStart: 300,
        scrolBreakTop: 1250,
        scrolBreakBottom: 1800,
        maxWidth: 700,
        svgdoc: null,
        waterdrop: null,
        svgdoc: null,
        clientWidth: null,
        waterDropTop: null,
        ripleCanvasTop: null,
        topMove: null,
        ripples:{
            canvas: $('.canvas.ripples'),
            x:230,
            y:150,
            repeatTimes: 3,
            repeat: 0,
        }
    },

    init: function(){
        waterdrop.data.svgdoc = waterdrop.data.svg.contentDocument;
        waterdrop.data.waterdrop = waterdrop.data.svgdoc.getElementById('water_drip');

        $(window).scroll( waterdrop.data, waterdrop.onScroll);
        $(window).resize( waterdrop.data, waterdrop.onUpdate);

        waterdrop.onUpdate({data: waterdrop.data});
    },

    onUpdate: function(event) {
        var data = event.data;
        var clientHeight = $(window).height();
        var scrollTop = $(window).scrollTop();

        data.waterDropTop = $('#water_drip_svg').height();
        
        data.clientWidth = $(window).width();

        data.topMove = data.ripleCanvasTop - data.waterDropTop+150;
       
        // console.log('waterdrop ', data.waterdrop);

        waterdrop.clearTimeline(data);

        data.tl.clear();
        data.tl
        .to(data.waterdrop, 0.1 , {autoAlpha:1})
        .to(data.waterdrop, data.time, {morphSVG:data.svgdoc.getElementById('water_drip_1')}, data.delay)
        .to(data.waterdrop, data.time, {morphSVG:data.svgdoc.getElementById('water_drip_2')}, data.delay)
        .to(data.waterdrop, data.time, {morphSVG:data.svgdoc.getElementById('water_drip_3')}, data.delay)
        .to(data.waterdrop, data.time, {morphSVG:data.svgdoc.getElementById('water_drip_4')}, data.delay)
        .to(data.waterdrop, data.time, {morphSVG:data.svgdoc.getElementById('water_drip_5')}, data.delay)
        .to(data.waterdrop, data.time, {morphSVG:data.svgdoc.getElementById('water_drip_6')}, data.delay)
        .to(data.waterdrop, data.time, {morphSVG:data.svgdoc.getElementById('water_drip_7')}, data.delay)
        .to(data.waterdrop, data.time/2, {morphSVG:data.svgdoc.getElementById('water_drip_8')}, data.delay)
        .to(data.waterdrop, data.time/2, {morphSVG:data.svgdoc.getElementById('water_drip_9')}, data.delay)
        .to(data.waterdrop, data.time/3, {morphSVG:data.svgdoc.getElementById('water_drip_10')}, '-=0')
        data.tl.stop();
    },

    onScroll: function(event) {
        var data = event.data;
        var width = $(window).width();

        //update
        waterdrop.data.scrollTop = $(window).scrollTop();
        waterdrop.data.clientHeight = $(window).height();

        var middle = (waterdrop.data.scrollTop) + waterdrop.data.clientHeight/2 - $(data.svg).height();
        var divide;

        switch(true) {
            case (width < 1080):
            divide = 1.3;
            break;  

            case (width<1380)&&(waterdrop.data.clientHeight>1280):
            divide = 1.05;
            break;

            case (width < 1280):
            divide = 1.3;
            break;

            case (width < 1380):
            divide = 1.5;
            break;  

            case (width < 1480):
            divide = 1.6;
            break;  

            case (width < 1680):
            divide = 1.9;
            break;

            case(navigator.userAgent.match(/iPad/i)):
            divide = 0.85;
            break;

            default:
            divide = 2;
            break;
        }

        console.log(waterdrop.data.clientHeight, width, divide);

        if((waterdrop.data.scrollTop + $(data.svg).height()/divide) > (data.ripples.canvas.offset().top + data.ripples.canvas.height()/2)){
            if(data.ripplesStarted || !data.aniStarted){
                return;
            }
            data.ripplesStarted = true;
            TweenMax.to(data.waterdrop, 0.5, {alpha: 0});
            waterdrop.startRipple(data.ripples);
            return;
        }

        if(!data.tl.isActive() && data.aniStarted && (waterdrop.data.scrollTop > !data.animateStart)){
            if(data.prevTop < waterdrop.data.scrollTop){
                if(middle > $(data.svg).offset().top){
                    TweenMax.to(data.svg, 0.7, {top: middle});
                }    
            }
        }
        if(waterdrop.data.scrollTop > data.animateStart && data.clientWidth > data.maxWidth){
            if(!data.reset && !data.tl.isActive()){
                data.aniStarted = true;
                data.tl.play();
            }
            data.reset = true;
        }

        if(waterdrop.data.scrollTop < data.animateStart){
            if(data.reset && !data.tl.isActive()){
                var top = ($(window).width() > 1480) ? -10 : -7;

              

                TweenMax.to(data.svg, 0.7, {top: top});
                TweenMax.to(data.waterdrop, 0, {alpha: 1});
                data.tl.to(data.waterdrop, 0, {morphSVG:data.svgdoc.getElementById('water_drip_1')}, 0);
                waterdrop.clearTimeline(data);
            }

            data.reset = false;
        }

        data.prevTop = waterdrop.data.scrollTop;
    },

    clearTimeline: function(data){
        data.aniStarted = false;
        data.ripplesStarted = false;
        data.tl.pause(0, true);
        data.tl.remove().invalidate();
    },

    startRipple: function(ripple){
        $(ripple.canvas).ripples({
            resolution: 140,
            // dropRadius: 1500,
            perturbance: 0.6,
            interactive: false
        });
        ripple.timeout = setTimeout(function(){
            waterdrop.createRipples(ripple);
        }, 300);
    },

    createRipples: function(ripple){
        var $el = $(ripple.canvas);
        var dropRadius = 25;
        var strength = .8;

        $el.ripples('drop', ripple.x, ripple.y, dropRadius, strength);

        ripple.repeat++;

        if(ripple.repeat === ripple.repeatTimes){
            ripple.repeat = 0;
            setTimeout(waterdrop.destroyRipples, 2800, ripple);
        }else{
            waterdrop.startRipple(ripple);
        }
    },

    destroyRipples: function(ripple){
        ripple.canvas.ripples('destroy');
    }
};