const scrollToTop = (scrollDuration) => {
  const cosParameter = window.scrollY / 2
  let scrollCount = 0
  let oldTimestamp = performance.now()
  const step = (newTimestamp) => {
    scrollCount += Math.PI / (scrollDuration / (newTimestamp - oldTimestamp))
    if (scrollCount >= Math.PI) window.scrollTo(0, 0)
    if (window.scrollY === 0) return
    window.scrollTo(0, Math.round(cosParameter + (cosParameter * Math.cos(scrollCount))))
    oldTimestamp = newTimestamp
    window.requestAnimationFrame(step)
  }
  window.requestAnimationFrame(step)
}

let lastPagePosition = window.pageYOffset
let headerInvisible = true
let windowWidth = window.innerWidth
let isDropImgMobile = windowWidth > 576
const rightDrop = document.getElementById('right_drop')
const rightDropInfo = document.getElementById('right_right_info')
const dropsSvg = document.getElementById('dropsInfographics')
const mainHeader = document.getElementById('main-header')
let offset = 500

const dropsInfographicsEl = document.querySelector('.comparing')
let infographicsTop
let animStarted
if (dropsInfographicsEl) {
  animStarted = false
  infographicsTop = dropsInfographicsEl.offsetTop - (dropsInfographicsEl.clientHeight)
}

const onScroll = () => {
  if (dropsInfographicsEl) {
    if (((infographicsTop - window.scrollY - dropsInfographicsEl.clientHeight) + offset) < 0 && !animStarted) {
      animStarted = true
      document.querySelector('body').classList.add('start')
      for (let index = 0; index < 10; index += 1) {
        const ddd = document.querySelector(`#anim0${index}`)
        ddd.beginElement()
      }
    }
    if (infographicsTop - (window.pageYOffset) > 250 && animStarted) {
      animStarted = false
      document.querySelector('body').classList.remove('start')
      for (let index = 0; index < 10; index += 1) {
        const ddd = document.querySelector(`#anim0${index}-1`)
        ddd.beginElement()
      }
    }
  }
  // header animation
  //if (headerInvisible && window.pageYOffset > lastPagePosition && window.pageYOffset > 120 && window.innerWidth > 758) {
  if (headerInvisible && window.pageYOffset > lastPagePosition && window.pageYOffset > 120) {
    mainHeader.classList.add('header-inactive')
    headerInvisible = false
  } else if ((!headerInvisible && window.pageYOffset < lastPagePosition) || window.pageYOffset < 121) {
    mainHeader.classList.remove('header-inactive')
    headerInvisible = true
  }
  lastPagePosition = window.pageYOffset
}

const changeDropsDiagram = () => {
    // swap out banner image
    var backdrop = $('.backdrop');
    var backdropSrc = $('.backdrop').attr('src');
    if(backdrop.length > 0){
        var backdropLargeDesktop = $('.backdrop').attr('data-large-desktop');
        var backdropDesktop = $('.backdrop').attr('data-desktop');
        var backdropMobile = $('.backdrop').attr('data-mobile');
        if (windowWidth < 701) {
            if(backdropSrc != backdropMobile){
                backdrop.attr('src', backdropMobile);
            }
        } else if (windowWidth < 2001) {
            if(backdropSrc != backdropDesktop){
                backdrop.attr('src', backdropDesktop);
            }
        } else {
            if(backdropSrc != backdropLargeDesktop){
                backdrop.attr('src', backdropLargeDesktop);
            }
        }
    }
  if (dropsInfographicsEl) {
    if (windowWidth < 577 && !isDropImgMobile) {
      isDropImgMobile = true
      rightDrop.setAttribute('transform', 'translate(-280,440)')
      rightDropInfo.setAttribute('transform', 'translate(-280,440)')
      dropsSvg.setAttribute('viewBox', '-30 -25 580 960')
      dropsSvg.setAttribute('height', '600px')
      offset = 800
    } else if (windowWidth > 576 && isDropImgMobile) {
      isDropImgMobile = false
      rightDrop.setAttribute('transform', 'translate(0,0)')
      rightDropInfo.setAttribute('transform', 'translate(0,0)')
      dropsSvg.setAttribute('viewBox', '-30 0 870 306')
      dropsSvg.setAttribute('height', '306px')
      offset = 500
    }
  }
}

const onResize = () => {
  if (dropsInfographicsEl) {
    infographicsTop = dropsInfographicsEl.offsetTop - (dropsInfographicsEl.clientHeight)
  }
  windowWidth = window.innerWidth
  changeDropsDiagram()
  onScroll()
}

const stopMovie = (currSlide) => {
  const slideType = currSlide.attr('class').split(' ')[1]
  const player = currSlide.find('iframe').get(0)
  let command
  if (slideType === 'vimeo') {
    command = {
      method: 'pause',
      value: 'true',
    }
  } else {
    command = {
      event: 'command',
      func: 'pauseVideo',
    }
  }
  if (player !== undefined) {
    player.contentWindow.postMessage(JSON.stringify(command), '*')
  }
}

onResize()

// Menu button
$('#nav-icon').on('click', function(e){
    e.preventDefault();
    if($(this).hasClass('is-active')){
        $(this).removeClass('is-active');
        $('#main-nav').removeClass('isMenuActive');
    } else {
        $(this).addClass('is-active');
        $('#main-nav').addClass('isMenuActive');
    }
})
// document.getElementById('nav-icon').onclick = function(e) {
//   e.preventDefault()
//   document.getElementById('main-nav').className = 'main-nav isMenuActive'
// }
// document.getElementById('close-nav-icon').onclick = function(e) {
//   e.preventDefault()
//   document.getElementById('main-nav').className = 'main-nav'
// }

window.addEventListener('resize', onResize)

document.addEventListener('scroll', onScroll)

const modal = document.getElementById('modal-overlay')
const modalBtn = document.getElementById('popup-btn')
const modal2 = document.getElementById('qr-modal-overlay')
const modal2Btn = $('.qrcode-btn')
const modal3 = document.getElementById('confirm-modal-overlay')
const modal3Btn = $('.main-contact-form')
const modalSpan = document.getElementsByClassName('close-testimonials')[0]
const modal2Span = document.getElementsByClassName('close-qr')[0]
const modal3Span = document.getElementsByClassName('confirm-close')[0]
const fireRefreshEventOnWindow = () => {
  const evt = document.createEvent('HTMLEvents')
  evt.initEvent('resize', true, false)
  window.dispatchEvent(evt)
}
if (modalBtn) {
  modalBtn.onclick = function(e) {
    e.preventDefault()
    modal.className = 'modal modal-active'
    // force slider refresh
    $('#movie-slider').slick('setPosition')
    $('#movie-slider-nav').slick('setPosition')
  }
}

if (modal2Btn) {
  modal2Btn.click(function(e) {
    e.preventDefault()
    modal2.className = 'qr-modal modal-active'
  })
}

if (modal3Btn) {
  modal3Btn.submit(function(evt) {
    evt.preventDefault()
    const x = document.forms['mainForm']
    if (x['field1'].value === ''
      || x['field2'].value === ''
      || x['field3'].value === ''
      || (x['field4'] !== undefined && x['field4'].value === '' )) {
      $('#confirm-modal-overlay p').html('请填写全部信息后再次提交')
      modal3.className = 'modal modal-active'
    } else {
      $('#confirm-modal-overlay p').html('提交成功')
      modal3.className = 'modal modal-active'
      $(this).trigger('reset')
    }
    return false
  })
}

if (modalBtn) {
  modalBtn.onclick = function() {
    modal.className = 'modal modal-active'
    // force slider refresh
    $('#movie-slider').slick('setPosition')
    $('#movie-slider-nav').slick('setPosition')
  }
}

if (modalSpan) {
  modalSpan.onclick = function() {
    stopMovie($('.slick-current'))
    modal.className = 'modal'
  }
}
if (modal2Span) {
  modal2Span.onclick = function() {
    modal2.className = 'qr-modal'
  }
}
if (modal3Span) {
  modal3Span.onclick = function() {
    modal3.className = 'modal'
  }
}
window.onclick = function(event) {
  if (event.target === modal) {
    stopMovie($('.slick-current'))
    modal.className = 'modal'
  } else if (event.target === modal2) {
    modal2.className = 'qr-modal'
  } else if (event.target === modal3) {
    modal3.className = 'modal'
  }
}

if (document.getElementById('map')) {
  var map
  function loadMapScenario() {
    map = new Microsoft.Maps.Map(document.getElementById('map'), {
        credentials: 'vE5UjgNRqovRXc1MkMED~jQzi0Otmfd2EQXp6AVHMiQ~ArANp5J5Hlb2nVpcW5JzsEAMwT-VOf-3lrhg9LNDyNXgsh6iBcIkYRWrYKuYA-Y5',
        zoom: 16,
        minZoom: 16,
        maxZoom: 16,
        center: new Microsoft.Maps.Location(53.705121, -6.361582),
    });
    var pushpin = new Microsoft.Maps.Pushpin(map.getCenter(), {
      icon: 'img/contact-pin.png',
      title: 'Donore Road, Drogheda, County Louth, Ireland',
    })
    map.entities.push(pushpin);
  }
}

$(document).ready(() => {
  $('#movie-slider').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    fade: true,
    adaptiveHeight: true,
    lazyLoad: 'ondemand',
    lazyLoadBuffer: 1,
    asNavFor: '#movie-slider-nav',
  })
  $('#movie-slider-nav').slick({
    slidesToShow: 7,
    slidesToScroll: 1,
    asNavFor: '#movie-slider',
    dots: false,
    centerMode: true,
    focusOnSelect: true,
    responsive: [
      {
        breakpoint: 720,
        settings: {
          slidesToShow: 5,
          slidesToScroll: 1,
        },
      },
      {
        breakpoint: 600,
        settings: {
          centerMode: true,
          slidesToShow: 3,
          slidesToScroll: 1,
        },
      },
      {
        breakpoint: 480,
        settings: {
          centerMode: false,
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
    ],
  })

  $('#movie-slider2').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    fade: true,
    adaptiveHeight: true,
    lazyLoad: 'ondemand',
    lazyLoadBuffer: 1,
    asNavFor: '#movie-slider-nav2',
  })
  $('#movie-slider-nav2').slick({
    slidesToShow: 4,
    slidesToScroll: 1,
    asNavFor: '#movie-slider2',
    dots: false,
    centerMode: false,
    focusOnSelect: true,
    responsive: [
      {
        breakpoint: 768,
        settings: {
          centerMode: false,
          slidesToShow: 3,
          slidesToScroll: 1,
        },
      },
      {
        breakpoint: 600,
        settings: {
          centerMode: false,
          slidesToShow: 2,
          slidesToScroll: 1,
        },
      },
    ],
  })

  $('#movie-slider').on('beforeChange', (event, slick) => {
    const currentSlide = $(slick.$slider).find('.slick-current')
    stopMovie(currentSlide)
  })

  // FAQ toggle animation
  $('.faq-title').click(function() {
    let options = {}
    $(this).parent().children('.faq-text').slideToggle(400)
    $(this).children('.faq-chevron').toggleClass('faq-chevron-up')
  })
})

// document.addEventListener('DOMContentLoaded', drawSite)
// window.loadTextures()
