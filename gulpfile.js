const gulp = require('gulp'),
  fs = require('fs'),
  source = require('vinyl-source-stream'),
  browserify = require('browserify'),
  uglify = require('gulp-uglify'),
  streamify = require('gulp-streamify'),
  babelify = require("babelify");
	gsap = require("gsap");
	glslify = require("glslify");

const watch = require('gulp-watch')
const batch = require('gulp-batch')
const sass = require('gulp-sass')

function compileJS(file) {
  browserify(`src/${file}.js`, { debug: true })
    .transform(babelify)
    .transform('glslify')
    .bundle()
    .on('error', (err) => { console.log(`Error : ${err.message}`) })
    // .pipe(source(file+'.min.js'))
    .pipe(source(`${file}.js`))
    .pipe(gulp.dest('build/js'))
}


gulp.task('wipe', () => {
  compileJS('wipe')
})

gulp.task('sass', () => {
  gulp.src(['build/css/*.scss'])
    .pipe(sass())
    .pipe(gulp.dest('build/css'))
})

gulp.task('default', ['sass'], () => {
  gulp.watch('build/css/*.scss', ['sass'])
})
