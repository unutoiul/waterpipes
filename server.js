const Hapi = require('hapi');
const Inert = require('inert');
const Vision = require('vision');
const ejs = require('ejs');
const Path = require('path');

const server = new Hapi.Server();

server.connection({
    port: 3000,
    routes: {
        cors: true,
        "state" : {
          	"parse": true,
          	"failAction": "ignore"
    	}
  	}
});

server.register([Vision, Inert], (err) => {
    if (err){
        console.error('Failed loading plugins');
        process.exit(1);
    }

});

server.views({
    engines: {
        html: ejs
    },
    path: Path.join(__dirname, 'build')
});


server.route({
	method: 'GET',
	path: '/',
	handler: function(req, res) {
		return res.view('index');
	}
});

server.route({
 	method: 'GET',
    path: '/{param*}',
    config: {
        auth: false
    },
    handler: {
        directory: {
            path: Path.join(__dirname, 'build'),
            index: false
        }
    }
});

server.start(() => {
    console.log('Server running at:', server.info.uri);
});



